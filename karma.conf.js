//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './app',

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'components/buy-button/*-module.js',
      'components/fish-list/*-module.js',
      'components/buy-button/*-component.js',
      'components/fish-list/*-component.js',
      'components/buy-button/*-spec.js',
      'components/fish-list/*-spec.js',
      'services/fish-service-module.js',
      'services/fish-service.js',
      'services/fish-service-spec.js'
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['Chrome'],

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter'
    ],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
