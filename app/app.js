'use strict';

// Declare app level module which depends on views, and components
angular.module('MoreFishyApp', [
    'fishList',
    'buyButton'
]);