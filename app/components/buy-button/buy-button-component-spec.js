describe('buyButton', function() {

    // Load the module that contains the `buyButton` component before each test
    beforeEach(module('buyButton'));

    // Mock fish service
    var fishServiceMock;
    var window;
    var scope;


    // define the mock Parse service
    beforeEach(function() {
        fishServiceMock = {
            getAllFishes: function() {},
            moreFishRelatedFunctions: function() {}
        };
    });

    // Test the controller
    describe('buyButtonController', function() {

        it('should create a buy button', inject(function($componentController, $window, $rootScope) {

            scope = $rootScope.$new();
            window = $window;

            var buyButtonController = $componentController('buyButton', {
                    $scope: scope,
                    fishService: fishServiceMock,
                    $window: window
                });

            expect(buyButtonController).toBeDefined();
        }));

    });

});