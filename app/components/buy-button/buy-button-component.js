/***
 * Angular Component for buy button re-use
 */

angular.
module('buyButton').
component('buyButton', {
    templateUrl: 'components/buy-button/buy-button-template.html',
    controller: function buyButtonController($scope, $window, fishService) {

        // Required for watcher
        $scope.fishService = fishService;

        /**
         * Depending on response from attest ether disable or enable button
         * Change color so its more obvious when its disabled
         */
        $scope.$watch('fishService.getResponseCheck()', function (newVal) {

            var button = angular.element(document.querySelector(".btn"));
            var tank = angular.element(document.querySelector("#tankPicture"));

            var colorGreen = "#00FF00";
            var colorGrey = "#C1C1C1";
            var colorRed = "#FF0000";

            if (newVal === true){
                // enable button
                button[0].disabled = false;
                button[0].style.background = "";
                // tank green
                tank[0].style.background = colorGreen;
            }else if (newVal === false) {
                // disable button
                button[0].disabled = true;
                button[0].style.background = colorGrey;
                // tank red
                tank[0].style.background = colorRed;
            }

        }, true);

        /**
         * Buy all the fishes in the tank
         */
        $scope.buyClicked = function() {
            $window.alert('BUY FISH!!!');
        };

    }
});