/***
 * Angular Component for listing fishes
 */

angular.
module('fishList').
component('fishList', {
    templateUrl: 'components/fish-list/fish-list-template.html',
    controller: function fishListController($scope, $http, fishService, $window) {
        // variable for all fishes
        this.allFishes = fishService.getAllFishes();
        // background color grey and green
        var backgroundColorGrey = "#E1E1E1";
        var backgroundColorGreen = "#00FF00";

        /**
         * Event called when fish is clicked
         * add to or remove from list of fishes in tank
         * add to or remove from list of all fishes for coloring
         * check that all fishes do not eat each other
         * @param event
         * @param fish
         */
        $scope.squareClicked = function(event, fish) {
            // Get element
            var element = angular.element(event.currentTarget);
            // change fish name to API name
            var fishName = fish.name.replace(" ","_");
            // boolean for add or remove
            var operation;

            // if fish already exists
            if ((fishService.getFish()).indexOf(fishName) !== -1){
                // remove operation
                operation = false;
                // already green - remove green
                element[0].style.backgroundColor = backgroundColorGrey;
                // remove fish
                fishService.removeFish(fishName);
                // set value in all fishes colour
                fish.color = backgroundColorGrey;
                fishService.updateAllFishes(fish)
            }else{
                // add operation
                operation = true;
                // not currently green - make green
                element[0].style.backgroundColor = backgroundColorGreen;
                // add fish into list in fish service
                fishService.addFish(fishName);
                // set value in all fishes colour
                fish.color = backgroundColorGreen;
                fishService.updateAllFishes(fish)
            }

            // check fishes will not each other
            $http({
                method: 'POST',
                url: 'https://fishshop.attest.tech/compatibility',
                data: {"fish":fishService.getFish()},
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function successCallback(responseData) {
                // add response to service
                fishService.setResponse(responseData.data.canLiveTogether);
            }, function errorCallback(responseData) {
                // Inform user that an error has occurred
                $window.alert("Something went wrong - Rolling back changes");
                // Roll back changes - Need to assume it could have been add or remove
                if (operation){
                    fishService.removeFish(fishName);
                    element[0].style.backgroundColor = backgroundColorGrey;
                    fish.color = backgroundColorGrey;
                    fishService.updateAllFishes(fish)
                }else{
                    fishService.addFish(fishName);
                    element[0].style.backgroundColor = backgroundColorGreen;
                    fish.color = backgroundColorGreen;
                    fishService.updateAllFishes(fish)
                }
            });
        };
    }
});
