describe('fishService', function() {

    var fishServiceImpl;

    // Load the module that contains the fish service before each test
    beforeEach(module('MoreFishyApp'));

    describe('when I call set response with true and then get response', function(){
        it('returns true', inject(function(fishService){
            fishServiceImpl = fishService;

            fishServiceImpl.setResponse(true);

            expect( fishServiceImpl.getResponse() ).toEqual(true);
        }))
    });

    describe('when I call add fish with a fish and then call get fish', function(){
        it('returns the same fish', inject(function(fishService){
            fishServiceImpl = fishService;
            var fish = { "name":"american crayfish", "color":"#E1E1E1"};

            fishServiceImpl.addFish(fish);

            expect( fishServiceImpl.getFish() ).toEqual([fish]);
        }))
    });

    describe('when I call add fish with many fish and then call get fish', function(){
        it('returns the list of all fishes', inject(function(fishService){
            fishServiceImpl = fishService;
            var fishOne = { "name":"american crayfish", "color":"#E1E1E1"};
            var fishTwo = {"name":"barreleye", "color":"#E1E1E1"};
            var fishThree = {"name":"batfish", "color":"#E1E1E1"};

            fishServiceImpl.addFish(fishOne);
            fishServiceImpl.addFish(fishTwo);
            fishServiceImpl.addFish(fishThree);

            expect( fishServiceImpl.getFish() ).toEqual([fishOne, fishTwo, fishThree]);
        }))
    });

    describe('when I call add fish with many fish and then call remove fish', function(){
        it('returns the list of fish without the removed one', inject(function(fishService){
            fishServiceImpl = fishService;
            var fishOne = { "name":"american crayfish", "color":"#E1E1E1"};
            var fishTwo = {"name":"barreleye", "color":"#E1E1E1"};
            var fishThree = {"name":"batfish", "color":"#E1E1E1"};

            fishServiceImpl.addFish(fishOne);
            fishServiceImpl.addFish(fishTwo);
            fishServiceImpl.addFish(fishThree);

            fishServiceImpl.removeFish(fishTwo);

            expect( fishServiceImpl.getFish() ).toEqual([fishOne, fishThree]);
        }))
    });

    describe('when I call get all fishes', function(){
        it('returns all the fishes', inject(function(fishService){
            fishServiceImpl = fishService;

            var allFish = fishServiceImpl.getAllFishes();

            expect( allFish ).toEqual([{ "name":"american crayfish", "color":"#E1E1E1"},  {"name":"barreleye", "color":"#E1E1E1"},  {"name":"batfish", "color":"#E1E1E1"},  {"name":"battered cod", "color":"#E1E1E1"},
                {"name":"betta splendens", "color":"#E1E1E1"},  {"name":"bonnethead", "color":"#E1E1E1"}, {"name":"cichlids", "color":"#E1E1E1"}, {"name":"cleaner shrimp", "color":"#E1E1E1"},
                {"name":"cocoa damselfish", "color":"#E1E1E1"}, {"name":"coelocanth", "color":"#E1E1E1"}, {"name":"cookiecutter", "color":"#E1E1E1"}, {"name":"cuttlefish", "color":"#E1E1E1"},
                {"name":"damselfish", "color":"#E1E1E1"}, {"name":"dragon wrasse", "color":"#E1E1E1"}, {"name":"electrophorus", "color":"#E1E1E1"}, {"name":"elephant seal", "color":"#E1E1E1"},
                {"name":"elvers", "color":"#E1E1E1"}, {"name":"fanfin seadevil", "color":"#E1E1E1"}, {"name":"fish fingers", "color":"#E1E1E1"}, {"name":"french angel fish", "color":"#E1E1E1"},
                {"name":"hammerhead", "color":"#E1E1E1"}, {"name":"harlequin shrimp", "color":"#E1E1E1"}, {"name":"hawksbill turtle", "color":"#E1E1E1"}, {"name":"megalodon", "color":"#E1E1E1"},
                {"name":"minnow", "color":"#E1E1E1"}, {"name":"neon tetra", "color":"#E1E1E1"}, {"name":"oarfish", "color":"#E1E1E1"}, {"name":"painted lobster", "color":"#E1E1E1"}, {"name":"prawn cocktail", "color":"#E1E1E1"},
                {"name":"psychedelic frogfish", "color":"#E1E1E1"}, {"name":"robocod", "color":"#E1E1E1"}, {"name":"salmon shark", "color":"#E1E1E1"}, {"name":"sand eel", "color":"#E1E1E1"},
                {"name":"sea lion", "color":"#E1E1E1"}, {"name":"shortfin mako shark", "color":"#E1E1E1"}, {"name":"slipper lobster", "color":"#E1E1E1"}, {"name":"sockeye salmon", "color":"#E1E1E1"},
                {"name":"spanish hogfish", "color":"#E1E1E1"}, {"name":"spinner dolphin", "color":"#E1E1E1"}, {"name":"stauroteuthis", "color":"#E1E1E1"}, {"name":"stingray", "color":"#E1E1E1"},
                {"name":"sunstar", "color":"#E1E1E1"}, {"name":"symphysodon", "color":"#E1E1E1"}, {"name":"torquigener", "color":"#E1E1E1"}]);
        }))
    });

    describe('when I call get all fishes and update a fish', function(){
        it('only updates that fish', inject(function(fishService){
            fishServiceImpl = fishService;

            var fishOne = { "name":"american crayfish", "color":"#C1C1C1"};

            fishServiceImpl.updateAllFishes(fishOne);

            expect( fishServiceImpl.getAllFishes() ).toEqual([fishOne,  {"name":"barreleye", "color":"#E1E1E1"},  {"name":"batfish", "color":"#E1E1E1"},  {"name":"battered cod", "color":"#E1E1E1"},
                {"name":"betta splendens", "color":"#E1E1E1"},  {"name":"bonnethead", "color":"#E1E1E1"}, {"name":"cichlids", "color":"#E1E1E1"}, {"name":"cleaner shrimp", "color":"#E1E1E1"},
                {"name":"cocoa damselfish", "color":"#E1E1E1"}, {"name":"coelocanth", "color":"#E1E1E1"}, {"name":"cookiecutter", "color":"#E1E1E1"}, {"name":"cuttlefish", "color":"#E1E1E1"},
                {"name":"damselfish", "color":"#E1E1E1"}, {"name":"dragon wrasse", "color":"#E1E1E1"}, {"name":"electrophorus", "color":"#E1E1E1"}, {"name":"elephant seal", "color":"#E1E1E1"},
                {"name":"elvers", "color":"#E1E1E1"}, {"name":"fanfin seadevil", "color":"#E1E1E1"}, {"name":"fish fingers", "color":"#E1E1E1"}, {"name":"french angel fish", "color":"#E1E1E1"},
                {"name":"hammerhead", "color":"#E1E1E1"}, {"name":"harlequin shrimp", "color":"#E1E1E1"}, {"name":"hawksbill turtle", "color":"#E1E1E1"}, {"name":"megalodon", "color":"#E1E1E1"},
                {"name":"minnow", "color":"#E1E1E1"}, {"name":"neon tetra", "color":"#E1E1E1"}, {"name":"oarfish", "color":"#E1E1E1"}, {"name":"painted lobster", "color":"#E1E1E1"}, {"name":"prawn cocktail", "color":"#E1E1E1"},
                {"name":"psychedelic frogfish", "color":"#E1E1E1"}, {"name":"robocod", "color":"#E1E1E1"}, {"name":"salmon shark", "color":"#E1E1E1"}, {"name":"sand eel", "color":"#E1E1E1"},
                {"name":"sea lion", "color":"#E1E1E1"}, {"name":"shortfin mako shark", "color":"#E1E1E1"}, {"name":"slipper lobster", "color":"#E1E1E1"}, {"name":"sockeye salmon", "color":"#E1E1E1"},
                {"name":"spanish hogfish", "color":"#E1E1E1"}, {"name":"spinner dolphin", "color":"#E1E1E1"}, {"name":"stauroteuthis", "color":"#E1E1E1"}, {"name":"stingray", "color":"#E1E1E1"},
                {"name":"sunstar", "color":"#E1E1E1"}, {"name":"symphysodon", "color":"#E1E1E1"}, {"name":"torquigener", "color":"#E1E1E1"}]);
        }))
    });

});