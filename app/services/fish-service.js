/***
 * Angular Service to allow for component interaction
 */

angular.
module('MoreFishyApp').
service('fishService', function() {
    // current list of fishes in tank
    var fishList = [];
    // list of all fishes and state
    var allFishes = [{ "name":"american crayfish", "color":"#E1E1E1"},  {"name":"barreleye", "color":"#E1E1E1"},  {"name":"batfish", "color":"#E1E1E1"},  {"name":"battered cod", "color":"#E1E1E1"},
        {"name":"betta splendens", "color":"#E1E1E1"},  {"name":"bonnethead", "color":"#E1E1E1"}, {"name":"cichlids", "color":"#E1E1E1"}, {"name":"cleaner shrimp", "color":"#E1E1E1"},
        {"name":"cocoa damselfish", "color":"#E1E1E1"}, {"name":"coelocanth", "color":"#E1E1E1"}, {"name":"cookiecutter", "color":"#E1E1E1"}, {"name":"cuttlefish", "color":"#E1E1E1"},
        {"name":"damselfish", "color":"#E1E1E1"}, {"name":"dragon wrasse", "color":"#E1E1E1"}, {"name":"electrophorus", "color":"#E1E1E1"}, {"name":"elephant seal", "color":"#E1E1E1"},
        {"name":"elvers", "color":"#E1E1E1"}, {"name":"fanfin seadevil", "color":"#E1E1E1"}, {"name":"fish fingers", "color":"#E1E1E1"}, {"name":"french angel fish", "color":"#E1E1E1"},
        {"name":"hammerhead", "color":"#E1E1E1"}, {"name":"harlequin shrimp", "color":"#E1E1E1"}, {"name":"hawksbill turtle", "color":"#E1E1E1"}, {"name":"megalodon", "color":"#E1E1E1"},
        {"name":"minnow", "color":"#E1E1E1"}, {"name":"neon tetra", "color":"#E1E1E1"}, {"name":"oarfish", "color":"#E1E1E1"}, {"name":"painted lobster", "color":"#E1E1E1"}, {"name":"prawn cocktail", "color":"#E1E1E1"},
        {"name":"psychedelic frogfish", "color":"#E1E1E1"}, {"name":"robocod", "color":"#E1E1E1"}, {"name":"salmon shark", "color":"#E1E1E1"}, {"name":"sand eel", "color":"#E1E1E1"},
        {"name":"sea lion", "color":"#E1E1E1"}, {"name":"shortfin mako shark", "color":"#E1E1E1"}, {"name":"slipper lobster", "color":"#E1E1E1"}, {"name":"sockeye salmon", "color":"#E1E1E1"},
        {"name":"spanish hogfish", "color":"#E1E1E1"}, {"name":"spinner dolphin", "color":"#E1E1E1"}, {"name":"stauroteuthis", "color":"#E1E1E1"}, {"name":"stingray", "color":"#E1E1E1"},
        {"name":"sunstar", "color":"#E1E1E1"}, {"name":"symphysodon", "color":"#E1E1E1"}, {"name":"torquigener", "color":"#E1E1E1"}];
    // Response from attest
    var response;

    /***
     * Add a fish to the list in the tank
     * @param fish
     */
    var addFish = function(fish){
        fishList.push(fish);
    };

    /**
     * Gets the list of fish in the tank
     * @returns {Array}
     */
    var getFish = function(){
        return fishList;
    };

    /***
     * Remove a fish from the list in the tank
     * @param fish
     */
    var removeFish = function(fish){
        for (var i=0;i<fishList.length;i++)
        {
            if (fishList[i] === fish)
            {
                fishList.splice(i, 1);
            }
        }
    };

    /**
     * Get all fishes
     * This should be available through the API really
     * @returns all fishes in JSON
     */
    var getAllFishes = function(){
        return allFishes;
    };

    /**
     * Update a single fish in allFishes
     * @param fish
     */
    var updateAllFishes = function(fish){
        for (var i=0;i<allFishes.length;i++)
        {
            if (allFishes[i].name === fish.name)
            {
                allFishes[i] = fish;
            }
        }
    };

    /**
     * Get the response of last result from attest
     * @returns true or false
     */
    var getResponse = function(){
        return response;
    };

    /***
     * Hack to allow watcher on current state of response
     * TODO: Look to find a observer and register callback
     * @returns true or false
     */
    var getResponseCheck = function(){
        return response;
    };

    /**
     * set the response of last result from attest
     * @param attestResponse
     */
    var setResponse = function(attestResponse){
        response = attestResponse;
        // Hack to allow watcher to correctly function
        var currentResponse = getResponseCheck();
    };

    /**
     * Available functions
     */
    return {
        updateAllFishes: updateAllFishes,
        addFish: addFish,
        getFish: getFish,
        getResponseCheck: getResponseCheck,
        removeFish: removeFish,
        getAllFishes: getAllFishes,
        getResponse: getResponse,
        setResponse: setResponse
    };

});