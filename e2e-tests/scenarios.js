'use strict';

describe('MoreFishyApp', function() {

    it('should load on index.html', function() {
        browser.get('index.html');
        expect(browser.getCurrentUrl()).toMatch("index.html");
    });

    it('On loading there will be 44 fish', function() {
        var todoList = element.all(by.repeater('fish in $ctrl.allFishes'));
        expect(todoList.count()).toEqual(44);
    });

    it('On typing in the search box american only one fish will be around', function() {
        element(by.id('searchBox')).sendKeys('american');
        var todoList = element.all(by.repeater('fish in $ctrl.allFishes'));
        expect(todoList.count()).toEqual(1);
    });

    it('On selecting bad fish the button should disable', function() {
        // TODO: Need to deal with unexpected alerts - currently have to accept yourself
        // TODO: best solution is to use http mock for end to end on protractor

        element(by.id('searchBox')).clear();
        browser.sleep(2000);
        element.all(by.repeater('fish in $ctrl.allFishes')).get(0).click();
        browser.sleep(3000);
        element.all(by.repeater('fish in $ctrl.allFishes')).get(1).click();
        browser.sleep(3000);
        element.all(by.repeater('fish in $ctrl.allFishes')).get(5).click();
        browser.sleep(3000);
        element.all(by.repeater('fish in $ctrl.allFishes')).get(10).click();
        browser.sleep(3000);
        expect(element(by.css('.btn')).getCssValue('background-color')).toEqual("rgba(193, 193, 193, 1)");
    });

});
